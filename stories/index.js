import { storiesOf } from '@storybook/vue'

import Logo from '../components/Logo'

storiesOf('Logo', module)
  .add('Simple Logo', () => (
    {
      components: { Logo },
      template: '<Logo />',
    }
  ))
