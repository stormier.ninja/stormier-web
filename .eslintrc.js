module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: [
    '@nuxtjs',
    'standard',
  ],
  rules: {
    'comma-dangle': [
      2,
      "always-multiline"
    ],
  },
}
