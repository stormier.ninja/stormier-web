#######################
# Step 1: Base target #
#######################
FROM node:10-slim as base
ENV HOST 0.0.0.0
# Base dir /app
WORKDIR /app


################################
# Step 2: "development" target #
################################
FROM base as development
COPY assets ./assets
COPY components ./components
COPY layouts ./layouts
COPY middleware ./middleware
COPY pages ./pages
COPY plugins ./plugins
COPY server ./server
COPY static ./static
COPY store ./store
COPY test ./test
COPY .gitignore .babelrc .eslintrc.js package-lock.json package.json nuxt.config.js jest.config.js ./
# Install app dependencies
RUN ["npm", "install"]

EXPOSE 3000

CMD ["npm","run", "dev"]


##########################
# Step 3: "build" target #
##########################
FROM development as build
ENV NPM_CONFIG_LOGLEVEL warn

RUN ["npm", "test"]
RUN ["npm", "run", "build"]

###############################
# Step 4: "production" target #
###############################
FROM build as production
ENV NODE_ENV=production
# Copy the transpiled code to use in production (in /app)
COPY --from=build /app/.nuxt ./.nuxt
COPY package.json package-lock.json ./
# Install production dependencies and clean cache
RUN npm install --production && \
    npm config set audit-level moderate && \
    npm audit --json --registry=https://registry.npmjs.org && \
    npm cache clean --force

CMD ["npm", "start"]
